import {NgxUiLoaderConfig, SPINNER} from 'ngx-ui-loader';

export const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  fgsType: 'folding-cube',
  pbThickness: 5,
  // fgsColor: '#69f0ae',
  // overlayColor: '#000',
  // pbColor: '#69f0ae',
  fgsColor: '#111',
  overlayColor: '#eee',
  pbColor: '#111',
  // text: 'De Luxe Delivery',
  // textColor: '#ffd600'
};
