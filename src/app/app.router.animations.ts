import {animate, animateChild, group, query, sequence, style, transition, trigger} from "@angular/animations";

// const query = (s, a, o = {optional: true}) => q(s, a, o);
//
// export const routerAnimations =
//   trigger('routerAnimations', [
//     // transition('* => *', [
//     //   query(':enter', style({opacity: 0})),
//     //   query(':leave', style({opacity: 1})),
//     //   query(':leave', animateChild()),
//     //   group([
//     //     query(':leave', [
//     //       animate('300ms ease-out', style({opacity: 0}))
//     //     ]),
//     //     query(':enter', [
//     //       animate('250ms 400ms ease-out', style({opacity: 1}))
//     //     ])
//     //   ]),
//     //   query(':enter', animateChild())
//     // ])
//     transition('* <=> *', [
//       query(':enter, :leave', style({position: 'fixed', opacity: '*'})),
//       group([
//         query(':enter', [
//           style({opacity: 0}),
//           animate('1200ms ease-in-out', style({opacity: 1}))
//         ]),
//         query(':leave', [
//           style({opacity: 1}),
//           animate('1200ms ease-in-out', style({opacity: 0}))]),
//       ])
//     ])
//   ]);

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* => *', [
    query(':enter', [
      style({opacity: 0})
    ], {optional: true}),
    query(':leave', [
      style({opacity: 1}), animate('0.3s', style({opacity: 0}))], {optional: true}),
    query(
      ':enter',
      [style({opacity: 0}), animate('0.3s', style({opacity: 1}))],
      {optional: true}
    )
  ])
]);



