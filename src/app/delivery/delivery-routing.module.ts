import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {CategoryComponent} from "./category";
import {DeliveryComponent} from "./delivery.component";
import {AuthComponent} from "./auth/auth.component";
import {SubcategoryComponent} from "./subcategory";
import {ProductComponent} from "./product";
import {AuthGuard} from "./core/services";
import {OrderComponent} from "./order";

const routes: Routes = [
  {
    path: '',
    component: DeliveryComponent,
    children: [
      {
        path: 'category',
        canActivate: [AuthGuard],
        component: CategoryComponent,
        data: {state: 'delivery-category'}
      },
      {
        path: 'subcategory',
        canActivate: [AuthGuard],
        component: SubcategoryComponent,
        data: {state: 'delivery-subcategory'}
      },
      {
        path: 'product',
        canActivate: [AuthGuard],
        component: ProductComponent,
        data: {state: 'delivery-product'}
      },
      {
        path: 'order',
        canActivate: [AuthGuard],
        component: OrderComponent,
        data: {state: 'delivery-order'}
      },
      {
        path: 'auth',
        component: AuthComponent,
        data: {state: 'delivery-auth'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule {}
