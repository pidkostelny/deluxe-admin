import {NgModule} from "@angular/core";
import {DeliveryComponent} from "./delivery.component";
import {DeliveryRoutingModule} from "./delivery-routing.module";
import {SharedModule} from "../shared";
import {CoreModule} from "./core";
import {AuthComponent} from "./auth/auth.component";
import {OrderComponent, UpdateOrderComponent, ProductItemComponent} from "./order";
import {CategoryComponent, CreateCategoryComponent} from "./category";
import {SubcategoryComponent, CreateSubcategoryComponent} from "./subcategory";
import {ProductComponent, CreateProductComponent} from "./product";

@NgModule({
  imports: [
    DeliveryRoutingModule,
    SharedModule
  ],
  declarations: [
    DeliveryComponent,
    OrderComponent,
    UpdateOrderComponent,
    ProductItemComponent,
    CategoryComponent,
    CreateCategoryComponent,
    SubcategoryComponent,
    CreateSubcategoryComponent,
    ProductComponent,
    CreateProductComponent,
    AuthComponent
  ],
  entryComponents: [
    UpdateOrderComponent,
    CreateCategoryComponent,
    CreateSubcategoryComponent,
    CreateProductComponent
  ],
  exports: [
    CoreModule
  ]
})
export class DeliveryModule {}
