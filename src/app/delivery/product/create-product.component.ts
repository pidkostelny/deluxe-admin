import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CategoryService, SubcategoryService} from "../core/services";
import {Category, Product, Subcategory} from "../core/models";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',

})
export class CreateProductComponent implements OnInit {

  createForm: FormGroup;
  categories: Category[] = [];
  subcategories: Subcategory[] = [];
  imgToDisplay = '/assets/img/no-image.svg';
  imgSource = null;

  constructor(private categoryService: CategoryService,
              private subcategoryService: SubcategoryService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateProductComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {}

  ngOnInit() {
    const el: Product = this.data.element || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required]],
      category: ['', [Validators.required]],
      sale: [!!el.sale],
      subcategory: [{value: '', disabled: true}, [Validators.required]],
      price: [el.price, [Validators.required]],
      weight: [el.weight, [Validators.required]],
      description: [el.description, [Validators.required]],
    });

    this.categoryService.getAll().subscribe(response => {
      this.categories = response;
      this.initValuesForUpdate(el);
    });
  }


  onCreateEntity() {
    const response = this.createForm.invalid && !this.sale.value ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      subCategoryId: this.subcategory.value.id,
      price: +this.price.value * 100,
      weight: +this.weight.value,
      img: this.imgSource,
      description: this.description.value,
      sale: this.sale.value
    };
    this.dialogRef.close(response);
  }

  onCategorySelected() {
    this.subcategoryService.getAllByCategoryId(this.category.value.id).subscribe(res => {
      this.setSubcategories(res, '');
    });
  }

  onPhotoSelected(target) {
    const file: File = target.files[0];
    const reader: FileReader = new FileReader();

    reader.onload = (e) => {
      this.imgSource = reader.result;
      this.imgToDisplay = this.imgSource;
    };
    reader.readAsDataURL(file);
  }

  private initValuesForUpdate(el: Product) {
    if (!el.id) return; //if create there no need to choose category
    this.imgToDisplay = `${environment.apiUrl}/images/${el.pathImage}`;
    this.category.setValue(this.categories.filter(c => c.id === el.subCategoryResponse.categoryResponse.id).shift());

    this.subcategoryService.getAllByCategoryId(this.category.value.id).subscribe(res => {
      this.setSubcategories(res, res.filter(c => c.id === el.subCategoryResponse.id).shift());
    });

  }

  private setSubcategories(subcategories, value) {
    this.subcategories = subcategories;
    this.subcategory.enable();
    this.subcategory.setValue(value);
  }

  get name() {
    return this.createForm.get('name');
  }

  get category() {
    return this.createForm.get('category');
  }

  get subcategory() {
    return this.createForm.get('subcategory');
  }

  get price() {
    return this.createForm.get('price');
  }

  get sale() {
    return this.createForm.get('sale');
  }

  get weight() {
    return this.createForm.get('weight');
  }

  get description() {
    return this.createForm.get('description');
  }

}
