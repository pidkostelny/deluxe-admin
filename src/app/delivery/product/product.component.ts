import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatSnackBar, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {Subscription} from 'rxjs';
import {Pagination, Product} from "../core/models";
import {ProductService} from "../core/services";
import {CreateProductComponent} from "./create-product.component";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  imagePrefix = `${environment.apiUrl}/images/`;

  noImageFilePath = '/assets/img/no-image.svg';

  searchFilter = '';

  displayedColumns: string[] = ['image', 'name', 'subcategoryName', 'price', 'weight', 'description', 'actions'];
  dataSource: MatTableDataSource<Product>;

  data$: Subscription;
  totalItems = 0;
  pageOptions: Pagination = new Pagination(0, 10, {fieldName: 'id', direction: 'DESC'});


  constructor(
    private productService: ProductService,
    private dialog: MatDialog,
    private snack: MatSnackBar
  ) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateProductComponent, {
      data: {element: element},
      minWidth: '80vw'
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.productService.update(response).subscribe(prod => this.updateData());
      } else if (response) {
        this.productService.create(response).subscribe(prod => this.updateData());
      }
    });
  }

  removeItem(id: number) {
    const submit = this.snack.open('Для видалення натисніть на кнопку', 'ВИДАЛИТИ',{
      duration: 3000
    });
    submit.onAction().subscribe(e => {
      this.productService.delete(id).subscribe(() => {
        this.updateData();
      });
    })
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.searchFilter.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.updateData();
  }

  applyFilter() {
    const search = this.searchFilter.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.productService.findByName(`${search}`, this.pageOptions).subscribe(data => this.applyDataFromApi(data));
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.productService.getPage(this.pageOptions).subscribe(data => this.applyDataFromApi(data))
  }

  private applyDataFromApi(data) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.totalElements;
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }
}
