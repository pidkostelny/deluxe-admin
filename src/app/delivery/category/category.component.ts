import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
import {Category, SeoInfo} from '../core/models';
import {Subscription} from 'rxjs';
import {CategoryService} from "../core/services";
import {CreateCategoryComponent} from "./create-category.component";
import {SeoInfoDialogComponent} from "../../shared/seo-info-dialog/seo-info-dialog.component";

@Component({
  selector: 'app-delivery-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'seoInfo', 'actions'];
  dataSource: MatTableDataSource<Category>;

  data$: Subscription;

  filterValue = '';

  constructor(
    private categoryService: CategoryService,
    private dialog: MatDialog,
    private snack: MatSnackBar
  ) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateCategoryComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.categoryService.update(response).subscribe(category => {
          this.updateData();
        });
      } else if (response) {
        this.categoryService.create(response).subscribe(category => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    const submit = this.snack.open('Для видалення натисніть на кнопку', 'ВИДАЛИТИ',{
      duration: 3000
    });
    submit.onAction().subscribe(e => {
      this.categoryService.delete(id).subscribe(() => {
        this.updateData();
      });
    });
  }

  showSeoInfo(category: Category) {
    this.dialog.open(SeoInfoDialogComponent, {
      data: {seoInfo: category.seoInfo, linkValue: category.linkValue}
    });
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.resetData();

      // this.data$ = this.categoryService.findOneByName(search).subscribe(data => {
      //
      //     this.dataSource = new MatTableDataSource(data);
      // });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.categoryService.getAll().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }
}
