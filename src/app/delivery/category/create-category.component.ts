import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Category} from "../core/models";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-delivery-create-category',
  templateUrl: './create-category.component.html',

})
export class CreateCategoryComponent implements OnInit {

  createForm: FormGroup;
  imgToDisplay = '/assets/img/no-image.svg';
  imgSource = null;


  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateCategoryComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    let el: any = this.data.element || {};
    this.initValuesForUpdate(el);
    el.seoInfo = el.seoInfo || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required] ],
      description: [el.seoInfo.description ],
      title: [el.seoInfo.title, [Validators.required] ],
      header: [el.seoInfo.header, [Validators.required] ],
      textContent: [el.seoInfo.textContent ],
      linkValue: [el.linkValue, [Validators.required] ]
    });
  }

  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      image: this.imgSource,
      seoInfo: {
        title: this.title.value,
        description: this.description.value,
        header: this.header.value,
        textContent: this.textContent.value
      },
      linkValue: this.linkValue.value
    };
    this.dialogRef.close(response);
  }

  onPhotoSelected(target) {
    const file: File = target.files[0];
    const reader: FileReader = new FileReader();

    reader.onload = (e) => {
      this.imgSource = reader.result;
      this.imgToDisplay = this.imgSource;
    };
    reader.readAsDataURL(file);
  }

  private initValuesForUpdate(el: Category) {
    if (!el.id) return; //if create there no need to set values
    this.imgToDisplay = `${environment.apiUrl}/categoryImages/${el.pathToImage}`;
  }

  get name() {
    return this.createForm.get('name');
  }

  get description() {
    return this.createForm.get('description');
  }

  get title() {
    return this.createForm.get('title');
  }

  get header() {
    return this.createForm.get('header');
  }

  get textContent() {
    return this.createForm.get('textContent');
  }

  get linkValue() {
    return this.createForm.get('linkValue');
  }

}
