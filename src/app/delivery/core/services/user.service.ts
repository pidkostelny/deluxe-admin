import {ApiService} from "./api.service";
import {JwtService} from "./jwt.service";
import {Injectable} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

@Injectable()
export class UserService {

  URL = '/public/authentication';

  constructor(
    private api: ApiService,
    private jwt: JwtService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  attemptAuth(credentials: {login: string, password: string}) {
    this.api.post(this.URL, credentials).subscribe(
      res => {
        this.jwt.saveToken(res.token);
        const prevPage = this.route.snapshot.queryParams['returnUrl'] || '/delivery';
        this.router.navigateByUrl(prevPage).then();
      },
      err => {
        alert("Помилка авторизації, спробуйте ще раз")
      }
    );

  }
}
