import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from './user.service';
import { take } from 'rxjs/operators';
import {JwtService} from "./jwt.service";

@Injectable()
export class AuthGuard implements CanActivate {

  previousUrl: string;

  constructor(
    private router: Router,
    private jwt: JwtService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {

    if (this.jwt.getToken()) {
      return true;
    } else {
      this.router.navigate(['/delivery/auth'], { queryParams: { returnUrl: state.url }}).then();
      return false;
    }
  }
}
