import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Category, Subcategory} from '../models';
import {Observable} from 'rxjs';
import {HttpParams} from "@angular/common/http";

@Injectable()
export class CategoryService {

  public static URL = 'category';

  constructor(private api: ApiService) {}

  getAll(): Observable<Array<Category>> {
    return this.api.get(`/public/${CategoryService.URL}`);
  }

  getOneById(categoryId: number): Observable<Category> {
    return this.api.get(`${CategoryService.URL}`, new HttpParams().set('id', categoryId.toString()));
  }

  create(category: Category): Observable<Category> {
    return this.api.post(CategoryService.URL, category);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(`${CategoryService.URL}/${id}`);
  }

  update(category: Category): Observable<Category> {
    return this.api.put(`${CategoryService.URL}/${category.id}`, category);
  }

  // findByName(name: string): Observable<Category> {
  //   return this.api.get(`${CategoryService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  // }
}
