import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import {catchError} from 'rxjs/internal/operators';
import {Router} from "@angular/router";
import {JwtService} from "./jwt.service";


@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private jwt: JwtService
  ) {}

  private formatErrors(error: any) {
    if (error.status === 401) {
      this.router.navigateByUrl('/delivery/auth').then(this.jwt.destroyToken);
    }
    return throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.apiUrl}${path}`, {
      params: params,
    }).pipe(catchError((e) => this.formatErrors(e)));
  }

  put(path: string, body: Object = {}, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body), {
        params: params
      }).pipe(catchError((e) => this.formatErrors(e)));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body))
      .pipe(catchError((e) => this.formatErrors(e)));
  }

  delete(path, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.delete(
      `${environment.apiUrl}${path}`, {
        params: params
      })
      .pipe(catchError((e) => this.formatErrors(e)));
  }

}
