import {Injectable} from "@angular/core";
import {ApiService} from "./api.service";
import {OrderSearchRequest, Statistic} from "../models";
import {Observable} from "rxjs";

@Injectable()
export class StatisticService {

  public static URL = 'statistic';

  constructor(private api: ApiService) {}

  getStatistic(request: OrderSearchRequest): Observable<Statistic> {
    return this.api.post(StatisticService.URL, request);
  }
}
