import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Subcategory} from '../models';
import {Observable} from 'rxjs';
import {HttpParams} from "@angular/common/http";

@Injectable()
export class SubcategoryService {

  public static URL = 'subCategory';

  constructor(private api: ApiService) {}

  getAll(): Observable<Array<Subcategory>> {
    return this.api.get(`/public/${SubcategoryService.URL}`);
  }

  getAllByCategoryId(categoryId: number): Observable<Array<Subcategory>> {
    return this.api.get(`/public/${SubcategoryService.URL}/${categoryId}`);
  }

  getOneById(subcategoryId: number): Observable<Subcategory> {
    return this.api.get(`${SubcategoryService.URL}`, new HttpParams().set('id', subcategoryId.toString()));
  }

  create(subcategory: Subcategory): Observable<Subcategory> {
    return this.api.post(SubcategoryService.URL, subcategory);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(`${SubcategoryService.URL}/${id}`);
  }

  update(subcategory: Subcategory): Observable<Subcategory> {
    return this.api.put(`${SubcategoryService.URL}/${subcategory.id}`, subcategory);
  }

  // findByName(name: string): Observable<Category> {
  //   return this.api.get(`${CategoryService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  // }
}
