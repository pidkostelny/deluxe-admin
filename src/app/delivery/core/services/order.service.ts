import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Order, OrderRequest, OrderSearchRequest, PageResponse, Pagination} from '../models';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class OrderService {

  public static URL = '/order';

  constructor(private api: ApiService) {}

  getAll(): Observable<Array<Order>> {
    return this.api.get(OrderService.URL);
  }

  getPage(pagination: Pagination): Observable<PageResponse<Order>> {
    return this.api.post(`${OrderService.URL}/page`, pagination);
  }

  getPageByFilter(filter: OrderSearchRequest): Observable<PageResponse<Order>> {
    return this.api.post(`${OrderService.URL}/search`, filter);
  }

  getPageByArchived(pagination: Pagination, archived: boolean) {
    return archived ? this.getPageArchived(pagination) : this.getPageUnarchived(pagination);
  }

  getPageArchived(pagination: Pagination): Observable<PageResponse<Order>> {
    return this.api.post(`${OrderService.URL}/page/archived`, pagination);
  }

  getPageUnarchived(pagination: Pagination): Observable<PageResponse<Order>> {
    return this.api.post(`${OrderService.URL}/page/unarchived`, pagination);
  }

  create(order: OrderRequest): Observable<Order> {
    return this.api.post(`/public/${OrderService.URL}`, order);
  }

  update(order: OrderRequest): Observable<Order> {
    return this.api.put(OrderService.URL, order);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(`${OrderService.URL}/${id}`);
  }

  archive(order: Order): Observable<Order> {
    return this.api.post(`/${OrderService.URL}/archived/${order.id}`);
  }

  unarchive(order: Order): Observable<Order> {
    return this.api.post(`/${OrderService.URL}/unarchived/${order.id}`);
  }

  search(value: string, pagination: Pagination): Observable<Order> {
    return this.api.get(`${OrderService.URL}/search`, this.httpParamsFromPagination(value, pagination));
  }

  private httpParamsFromPagination(value: string, pagination: Pagination): HttpParams {
    return new HttpParams()
      .set('value', `%${value}%`)
      .set('page', pagination.page.toString())
      .set('size', pagination.size.toString())
      .set('sortBy', pagination.sort.fieldName)
      .set('direction', pagination.sort.direction);
  }
}
