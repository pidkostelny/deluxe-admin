import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {PageResponse, Pagination, Product} from '../models';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class ProductService {

  public static URL = 'goods';

  constructor(private api: ApiService) {}

  getAll(): Observable<Array<Product>> {
    return this.api.get(`/public/${ProductService.URL}`);
  }

  getAllSales(): Observable<Array<Product>> {
    return this.api.get(`/public/${ProductService.URL}/sales`);
  }

  getAllByCategory(categoryId: number): Observable<Array<Product>> {
    return this.api.get(`/public/${ProductService.URL}/by/category/${categoryId}`);
  }

  getAllBySubcategory(subcategoryId: number): Observable<Array<Product>> {
    return this.api.get(`/public/${ProductService.URL}/by/subCategory/${subcategoryId}`);
  }

  getPage(pagination: Pagination): Observable<PageResponse<Product>> {
    return this.api.post(`/public/${ProductService.URL}/page`, pagination);
  }

  getPageByCategory(id: number, pagination: Pagination): Observable<PageResponse<Product>> {
    return this.api.post(`/public/${ProductService.URL}/page/by/category/${id}`, pagination);
  }

  getPageBySubcategory(id: number, pagination: Pagination): Observable<PageResponse<Product>> {
    return this.api.post(`/public/${ProductService.URL}/page/by/subcategory/${id}`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(`${ProductService.URL}/${id}`);
  }

  create(product: Product): Observable<Product> {
    return this.api.post(ProductService.URL, product);
  }

  update(product: Product): Observable<Product> {
    return this.api.put(`${ProductService.URL}/${product.id}`, product);
  }

  findByName(name: string, pagination: Pagination): Observable<PageResponse<Product>> {
    return this.api.post(`/public/${ProductService.URL}/search?name=${name}`, pagination);
  }
}
