import {NgModule} from '@angular/core';

import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpTokenInterceptor} from "./interceptors";

import {
  AuthGuard,
  ApiService,
  CartService,
  CategoryService,
  JwtService,
  OrderService,
  StatisticService,
  SubcategoryService,
  ProductService,
  UserService
} from './services';

@NgModule({
  imports: [

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    ApiService,
    AuthGuard,
    CartService,
    CategoryService,
    JwtService,
    OrderService,
    SubcategoryService,
    StatisticService,
    ProductService,
    UserService
  ]
})
export class CoreModule {}
