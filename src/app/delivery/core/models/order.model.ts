import {User} from './user.model';
import {ProductForOrder} from './product-for-order.model';
import {Time} from "@angular/common";

export interface Order {
  archive: boolean;
  description: string;
  sum: number;
  deliveryPrice: number;
  userResponse: User;
  goodsResponseList: Array<ProductForOrder>;
  id?: number;
  date?: Date;
  time?: Time;
}
