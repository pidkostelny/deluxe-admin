import {Pagination} from "./pagination.model";

export interface OrderSearchRequest {
  sumFrom: number;
  sumTo: number;
  dateFrom: string;
  dateTo: string;
  archived: boolean;
  value: string;
  pagination: Pagination;
}
