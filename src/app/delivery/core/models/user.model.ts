
export interface User {
  name: string;
  number: string;
  address: string;
  addressNumber: string;
  surName?: string;
}
