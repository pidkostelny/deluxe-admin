import {Category} from './category.model';
import {SeoInfo} from "./seo-info.model";

export interface Subcategory {
  id: number;
  name: string;
  seoInfo: SeoInfo;
  categoryResponse: Category;
  linkValue: string;
}
