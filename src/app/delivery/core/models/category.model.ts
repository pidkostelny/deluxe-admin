import {SeoInfo} from "./seo-info.model";


export interface Category {
  id: number;
  name: string;
  seoInfo: SeoInfo;
  subcategoriesCount: number;
  pathToImage: string;
  linkValue: string;
}
