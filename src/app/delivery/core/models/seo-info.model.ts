export interface SeoInfo {
  title: string;
  description: string;
  header: string;
  textContent: string;
}
