import {User} from "./user.model";

export interface OrderRequest {
  archive: boolean;
  description: string;
  sum: number;
  user: User;
  goods: Array<{count: number, idGoods: number}>;
  id?: number;
}
