import {Subcategory} from './subcategory.model';

export interface ProductForOrder {
  id: number;
  name: string;
  price: number;
  weight: number;
  description: string;
  pathImage: string;
  count: number;
  subCategoryResponse: Subcategory;
}
