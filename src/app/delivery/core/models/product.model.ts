import {Subcategory} from './subcategory.model';

export interface Product {
  id: number;
  name: string;
  price: number;
  weight: number;
  description: string;
  pathImage: string;
  subCategoryResponse: Subcategory;
  sale: boolean;
  count?: number;
}
