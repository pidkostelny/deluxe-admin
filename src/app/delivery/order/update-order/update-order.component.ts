import {Component, Inject, OnDestroy, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Observable, Subscription} from "rxjs";
import {map} from 'rxjs/operators';
import {environment} from "../../../../environments/environment";
import {OrderRequest, Pagination, Product} from "../../core/models";
import {OrderService, ProductService} from "../../core/services";

@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.scss']
})
export class UpdateOrderComponent implements OnInit, OnDestroy {

  private $valueChanges: Subscription;

  apiUrl = environment.apiUrl;
  productSearchCtrl = new FormControl();
  order;

  addedProducts: Array<Product> = [];

  products: Observable<Product[]>;


  constructor(
    private dialogRef: MatDialogRef<UpdateOrderComponent>,
    @Inject(MAT_DIALOG_DATA) private data,
    private productService: ProductService,
    private orderService: OrderService
              ) {}

  ngOnInit(): void {
    this.order = this.data['order'] || {};
    this.$valueChanges = this.productSearchCtrl.valueChanges.subscribe(value => {
      this.products = this.getProducts(value);
    })
  }

  ngOnDestroy(): void {
    this.$valueChanges.unsubscribe();
  }

  getProducts(value): Observable<Product[]> {
    return this.productService.findByName(value, new Pagination()).pipe(
      map(page => page.content)
    )
  }

  optionSelected(product: Product) {
    if (this.order.goodsResponseList.find(e => e.goodsId === product.id)) {
      return;
    }
    product.count = 1;
    this.addedProducts.push(product);
    this.productSearchCtrl.reset();
  }

  onSubmit() {
    const request: OrderRequest = {
      user: this.order.userResponse,
      archive: this.order.archive,
      description: this.order.description,
      sum: this.order.sum,
      id: this.order.id,
      goods: []
    };
    this.order.goodsResponseList.forEach(e => e.count && request.goods.push({idGoods: e.goodsId, count: e.count}));
    this.addedProducts.forEach(e => e.count &&  request.goods.push({idGoods: e.id, count: e.count}));
    this.orderService.update(request).subscribe(e => {
      this.dialogRef.close();
    })
  }

  displayFunc(product: Product) {
    return product ? product.name : undefined;
  }

}
