import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {ProductForOrder} from "../../../core/models";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent {

  apiUrl = environment.apiUrl;
  @Input() product: ProductForOrder;

  incrementCount(product) {
    product.count++;
  }

  decrementCount(product) {
    if (product.count > 0) {
      product.count--;
    }
  }
}
