import {Component, OnInit, ViewEncapsulation} from '@angular/core';


import {MatDialog, MatSnackBar, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {Subscription} from 'rxjs';
import {OrderService, StatisticService} from "../core/services";
import {Pagination, Statistic} from "../core/models";
import {UpdateOrderComponent} from "./update-order/update-order.component";
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {

  showFilters = false;
  showStats = false;

  private statisticLoading$: Subscription;
  showLoading = false;
  statistic: Statistic;

  private orders: any;
  searchFilter = '';
  filterForm: FormGroup;
  enableFilter = false;

  displayedColumns: string[] = ['id', 'client', 'phoneNumber', 'date', 'order', 'description', 'sum', 'actions'];
  dataSource: MatTableDataSource<any>;

  data$: Subscription;
  totalItems = 0;
  archived = false;
  pageOptions: Pagination = new Pagination(0, 10, {fieldName: 'id', direction: 'DESC'});


  constructor(
    private orderService: OrderService,
    private statisticService: StatisticService,
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.initFilterForm();
    this.orderService.getPageUnarchived(this.pageOptions).subscribe(data => {
      this.orders = data.content;
    });
    this.updateData();
  }

  updateOrder(order) {
    const dialogRef = this.dialog.open(UpdateOrderComponent, {
      data: {order: order}
    });

    dialogRef.afterClosed().subscribe(response => {
      this.updateData();
    });
  }

  archiveOrder(order){
    const response =  order.archive ? this.orderService.unarchive(order) : this.orderService.archive(order);
    response.subscribe(() => {
      this.updateData();
    });
  }

  deleteOrder(id){
    const submit = this.snack.open('Для видалення натисніть на кнопку', 'ВИДАЛИТИ',{
      duration: 3000
    });
    submit.onAction().subscribe(e => {
      this.orderService.delete(id).subscribe(() => this.updateData());
    })
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'DESC';
    this.pageOptions.sort.fieldName = event.active || 'date';
    this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.updateData();
  }

  archivedChange(checked: boolean) {
    this.archived = checked;
    this.updateData();
  }

  onShowStatisticChange(state: boolean) {
    if (state) {
      this.showFilters = true;
    }
    state ? this.sumTo.disable() : this.sumTo.enable();
    state ? this.sumFrom.disable() : this.sumFrom.enable();
    this.updateData();
  }

  onShowFilterChange(state: boolean) {
    if (!state) {
      this.showStats = false;
      this.onResetClick();
    }
    this.updateData()
  }

  onSearchClick() {
    this.enableFilter = true;
    this.updateData();
  }

  onResetClick() {
    this.enableFilter = false;
    this.filterForm.reset();
    this.searchFilter = '';
    this.updateData();
  }

  getDataByFilter() {
    this.pageOptions.page = 0;
    return this.orderService.getPageByFilter(this.filter)
  }

  private updateData() {
    this.resetData();
    this.updateStatistic();
    if (this.enableFilter) {
      this.data$ = this.getDataByFilter().subscribe(data => this.applyDataFromApi(data));
    } else {
      this.data$ = this.orderService.getPageByArchived(this.pageOptions, this.archived).subscribe(data => this.applyDataFromApi(data));
    }
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private applyDataFromApi(data) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.totalElements;
  }

  private updateStatistic() {
    this.showLoading = true;
    if (this.showStats && this.enableFilter) {
      this.statisticLoading$ = this.statisticService.getStatistic(this.filter).subscribe(stat => {
        this.statistic = stat;
        this.showLoading = false;
      });
    } else {
      this.showLoading = false;
      this.statistic = new Statistic();
      if (this.statisticLoading$) {
        this.statisticLoading$.unsubscribe();
      }
    }
  }


  private initFilterForm() {
    this.filterForm = this.fb.group({
      dateFrom: [''],
      dateTo: [''],
      sumFrom: [''],
      sumTo: ['']
    })
  }

  get filter() {
    return {
      archived: this.archived,
      dateFrom: this.dateFrom.value ? this.dateFrom.value : null,
      dateTo: this.dateTo.value ? this.dateTo.value : null,
      sumFrom: this.sumFrom.value && this.sumFrom.enabled ? this.sumFrom.value * 100 : null,
      sumTo: this.sumTo.value && this.sumTo.enabled ? this.sumTo.value * 100 : null,
      value: this.searchFilter.trim(),
      pagination: this.pageOptions
    }
  }

  get dateFrom() {
    return this.filterForm.get('dateFrom');
  }

  get dateTo() {
    return this.filterForm.get('dateTo');
  }

  get sumFrom() {
    return this.filterForm.get('sumFrom');
  }

  get sumTo() {
    return this.filterForm.get('sumTo');
  }
}
