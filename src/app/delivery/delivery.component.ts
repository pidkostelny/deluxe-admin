import { Component, OnInit } from '@angular/core';
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {fadeAnimation} from "../app.router.animations";
import {RouterOutlet} from "@angular/router";

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss'],
  animations: [
    fadeAnimation
  ]
})
export class DeliveryComponent implements OnInit {

  routes = [
    { path: 'order', name: 'Замовлення'},
    { path: 'category', name: 'Категорії'},
    { path: 'subcategory', name: 'Підатегорії'},
    { path: 'product', name: 'Товари'},
  ];

  categories = [
    { name: 'Європейська кухня', description: '', img: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Indiandishes.jpg'},
    { name: 'Європейська кухня', description: '', img: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Indiandishes.jpg'},
    { name: 'Європейська кухня', description: '', img: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Indiandishes.jpg'},
    { name: 'Європейська кухня', description: '', img: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Indiandishes.jpg'},
  ]

  constructor() { }

  ngOnInit() {
  }


}
