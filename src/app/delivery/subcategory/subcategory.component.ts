import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {SeoInfo, Subcategory} from "../core/models";
import {SubcategoryService} from "../core/services";
import {CreateSubcategoryComponent} from "./create-subcategory.component";
import {SeoInfoDialogComponent} from "../../shared/seo-info-dialog/seo-info-dialog.component";

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})
export class SubcategoryComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'categoryName', 'seoInfo', 'actions'];
  dataSource: MatTableDataSource<Subcategory>;

  data$: Subscription;


  constructor(
    private subcategoryService: SubcategoryService,
    private dialog: MatDialog,
    private snack: MatSnackBar
  ) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateSubcategoryComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.subcategoryService.update(response).subscribe(subType => {
          this.updateData();
        });
      } else if (response) {
        this.subcategoryService.create(response).subscribe(subType => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    const submit = this.snack.open('Для видалення натисніть на кнопку', 'ВИДАЛИТИ',{
      duration: 3000
    });
    submit.onAction().subscribe(e => {
      this.subcategoryService.delete(id).subscribe(() => {
        this.updateData();
      });
    });
  }

  showSeoInfo(subcategory: Subcategory) {
    this.dialog.open(SeoInfoDialogComponent, {
      data: {seoInfo: subcategory.seoInfo, linkValue: subcategory.linkValue}
    });
  }

  private updateData() {
    this.resetData();
    this.data$ = this.subcategoryService.getAll().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }
}
