import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CategoryService} from "../core/services";
import {Category} from "../core/models";

@Component({
  selector: 'app-create-subcategory',
  templateUrl: './create-subcategory.component.html',

})
export class CreateSubcategoryComponent implements OnInit {

  createForm: FormGroup;
  categories: Category[] = [];

  constructor(private categoryService: CategoryService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateSubcategoryComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {}

  ngOnInit() {
    const el: any = this.data.element || {};
    el.seoInfo = el.seoInfo || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required]],
      category: ['', [Validators.required]],
      description: [el.seoInfo.description ],
      title: [el.seoInfo.title, [Validators.required] ],
      header: [el.seoInfo.header, [Validators.required] ],
      textContent: [el.seoInfo.textContent ],
      linkValue: [el.linkValue, [Validators.required] ]
    });

    this.categoryService.getAll().subscribe(response => {
      this.categories = response;

      this.initValuesForUpdate(el);
    });
  }


  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      categoryId: this.category.value.id,
      seoInfo: {
        title: this.title.value,
        description: this.description.value,
        header: this.header.value,
        textContent: this.textContent.value
      },
      linkValue: this.linkValue.value
    };
    this.dialogRef.close(response);
  }

  private initValuesForUpdate(el: any) {
    if (!el.id) return; //if create there no need to choose category
    this.category.setValue(this.categories.filter(c => c.id === el.categoryResponse.id).shift());
  }

  get name() {
    return this.createForm.get('name');
  }

  get category() {
    return this.createForm.get('category');
  }

  get description() {
    return this.createForm.get('description');
  }

  get title() {
    return this.createForm.get('title');
  }

  get header() {
    return this.createForm.get('header');
  }

  get textContent() {
    return this.createForm.get('textContent');
  }

  get linkValue() {
    return this.createForm.get('linkValue');
  }
}
