import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Router} from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavComponent implements OnInit {

  routes = [
    {path: '/delivery', name: 'Доставка Їжі', disabled: false},
    {path: '/Alcohol', name: 'Алкоголь', disabled: true},
  ];

  constructor(
    private dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit() {
  }

  onLogInClick() {
    // this.dialog.open(AuthComponent);
  }
}
