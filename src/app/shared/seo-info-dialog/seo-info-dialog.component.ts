import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-seo-info-dialog',
  templateUrl: './seo-info-dialog.component.html',

})
export class SeoInfoDialogComponent implements OnInit {

  seoInfo;
  linkValue;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<SeoInfoDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    this.seoInfo = this.data['seoInfo'] || {};
    this.linkValue = this.data['linkValue'];
  }

}
