import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {
  MatCheckboxModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatSliderModule,
  MatInputModule,
  MatBadgeModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatTooltipModule,
  MatTabsModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatSlideToggleModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule
} from '@angular/material';

import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {SeoInfoDialogComponent} from "./seo-info-dialog/seo-info-dialog.component";
import {PricePipe} from "./pipes";

@NgModule({
  imports: [
    MatIconModule, MatInputModule, MatButtonModule, MatRadioModule, MatSliderModule, MatCheckboxModule, MatBadgeModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatTooltipModule, MatTabsModule, MatCardModule, MatMenuModule, MatToolbarModule, MatPaginatorModule, MatSortModule, MatTableModule, MatSlideToggleModule, MatAutocompleteModule, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    SeoInfoDialogComponent,
    PricePipe
  ],
  entryComponents: [
    SeoInfoDialogComponent
  ],
  exports: [
    MatIconModule, MatInputModule, MatButtonModule, MatRadioModule, MatSliderModule, MatCheckboxModule, MatBadgeModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatTooltipModule, MatTabsModule, MatCardModule, MatMenuModule, MatToolbarModule, MatPaginatorModule, MatSortModule, MatTableModule, MatSlideToggleModule, MatAutocompleteModule, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PricePipe
    // SeoInfoDialogComponent
  ]
})
export class SharedModule {}
